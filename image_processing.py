import cv2
import numpy as np
import os
import imghdr
def calculate_area(vertices):
    n = len(vertices)
    area = 0

    for i in range(n):
        j = (i + 1) % n
        area += (vertices[i][0] * vertices[j][1]) - (vertices[j][0] * vertices[i][1])
    area=abs(area)/2
    return area
def get_top_4_highest_neighborhoods(image):
    # Get the height and width of the image
    image_height, image_width = image.shape

    # Define the 5x5 kernel size
    kernel_size = 5

    # Create a list to store neighborhood information
    neighborhoods_info = []
#    count=0
    # Iterate over the image with the non-overlapping 5x5 kernel
    for y in range(0, image_height, kernel_size):
        for x in range(0, image_width, kernel_size):
            # Extract the 5x5 neighborhood at the current position (x, y)
            neighborhood = image[y:y + kernel_size, x:x + kernel_size]
            neighborhood_avg = np.mean(neighborhood)

            # Store the neighborhood information as a tuple (y, x, avg, neighborhood)
            neighborhoods_info.append((y, x, neighborhood_avg, neighborhood))

#Un comment only in case of experiment 1 to conferm that there is no patch overlapping
#            count=count+1
#    assert count == 36, "Patches are overlapping."
#    print("TEST 4 PASSED: Patches of window size 5x5 are not overlapping.")
    # Sort the neighborhoods_info list based on the neighborhood_avg in descending order
    neighborhoods_info.sort(key=lambda x: x[2], reverse=True)

    # Get the top 4 highest neighborhood_avg values and their corresponding neighborhoods
    top_4_highest = neighborhoods_info[:4]

    # Store the central pixel positions for forming quadrilaterals
    quadrilateral_pts = []

    for y, x, avg, neighborhood in top_4_highest:
        central_y, central_x = y + 2, x + 2  # Position of the central pixel
        central_pixel_value = image[central_y, central_x]
        print("Top Neighborhood Avg: {}".format(avg))
        print("Position (y, x) of Central Pixel: ({}, {})".format(central_y, central_x))
        print("Value of Central Pixel: {}".format(central_pixel_value))
        print("Corresponding Neighborhood:")
        print(neighborhood)
        print("---------------")

        # Add the central pixel position to the list for forming quadrilaterals
        quadrilateral_pts.append((central_x, central_y))

    # Form the quadrilaterals by connecting the central pixel positions
    if len(quadrilateral_pts) == 4:
        pts = np.array(quadrilateral_pts, np.int32)
        pts = pts.reshape((-1, 1, 2))
        # Calculate the area of the quadrilateral
        area = calculate_area(quadrilateral_pts)
        # Test Case 2: Check if the quadrilateral has exactly four vertices
        assert len(quadrilateral_pts) == 4, "The formed quadrilateral does not have exactly four vertices."
        print("TEST 2 PASSED: The quadrilateral has exactly four vertices.")
        # Test Case 2: Test case for checking the area of the quadrilateral (un comment only in case of experiment 1 because the expected/ground truth area is know)
  #       expected_area = 225
  #      assert area == expected_area, f"Area is {area}, but expected {expected_area}."
  #     print("TEST 5 PASSED: Area of the Quadrilateral is correct (225 pixels^2).")

        # Print the area of the quadrilateral in pixels
        print("Area of the Quadrilateral: {} pixels^2".format(area))

        # Convert the grayscale image to a color image with a red channel
        image_color = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

        # Draw the red quadrilateral
        color = (0, 0, 255)  # Red color in BGR format
        thickness = 1
        image_with_quadrilaterals = cv2.polylines(image_color, [pts], True, color, thickness)

        # Save the image with the red quadrilateral in PNG format
        output_filename = "/home/labdoo/Downloads/lenababy/output/output_image.png"
        cv2.imwrite(output_filename, image_with_quadrilaterals)
        #Test to check if the file is succesfully saved as .png format
        print("Image with Quadrilateral saved as {}".format(output_filename))

        assert os.path.exists(output_filename), "Image file not found."
        assert imghdr.what(output_filename) == "png", "Image file is not in PNG format."

        print("TEST 3 PASSED: Image with red quadrilateral is saved in PNG format.")
        # Display the image with the red quadrilateral
        cv2.imshow("Image with Quadrilateral", image_with_quadrilaterals)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

if __name__ == "__main__":
    # Read the image as grayscale
    image = cv2.imread("/home/labdoo/Downloads/lenababy/lena2.png", cv2.IMREAD_GRAYSCALE)
    # TEST CASE 1: Test if the image is read successfully
    assert image is not None, "Failed to read the image."

    print("TEST 1 PASSED: Image read successfully and is in grayscale format.")

    # Call the function to get and print the top 4 highest neighborhood_avg values,
    # their corresponding neighborhoods, and the values of central pixels, and display the image
    get_top_4_highest_neighborhoods(image)
